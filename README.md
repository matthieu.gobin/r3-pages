
# R3-pages (Legacy, replaced by R3/outreach/r3lab!)

This is a repository containing source for the index pages of [http://r3lab.uni.lu/](http://r3lab.uni.lu/) and its frozen pages.

Note that the sites from _Websites_ menu tab, are contained in their own repositories:
  * TGM-pipeline at [https://gitlab.lcsb.uni.lu/core-services/r3lab/tgm-pipeline-doc]()
  * CASian at [https://gitlab.lcsb.uni.lu/core-services/r3lab/casian-doc](https://gitlab.lcsb.uni.lu/core-services/r3lab/casian-doc)
  * IMP at [https://gitlab.lcsb.uni.lu/core-services/r3lab/imp-doc](https://gitlab.lcsb.uni.lu/core-services/r3lab/imp-doc)

# Deployment
## How are the pages built?
All of these pages are built with Jekyll using Gitlab-CI (see respective `.gitlab-ci.yml` files and the instructions on [https://gitlab.lcsb.uni.lu/core-services/jekyll-theme-lcsb-default](https://gitlab.lcsb.uni.lu/core-services/jekyll-theme-lcsb-default)` if you need more information).

## How are the pages uploaded to the VM?
There is `deploy to production` section in `.gitlab-ci.yml`, which pushes the built webpage onto the VM. Under the hood, there are details saved in [Settings ==> CI/CD ==> Variables](https://gitlab.lcsb.uni.lu/core-services/r3lab/r3-pages/-/settings/ci_cd) section, which comprise the secret values (SSH key, the username, VM address etc.). If you create a new page which should upload the page there, don't forget to add those!

## How are the pages published?
At current moment, the staticpages VM is configured to expose files from `/home/r3pages/web/latest` under `r3lab.uni.lu`.

  * the index page goes into `~/web/web_$CI_TASK_ID` (e.g. `~/web/web_115885`) and then it gets soft-symlinked into `~/web/latest`.
  * the other pages go into `~/web/$PROJECT_NAME_$CI_TASK_ID` (e.g. `~/web/casian-doc_115903`), and then it gets soft-symlinked into `~/web/latest/web/$PROJECT_NAME`, e.g. `~/web/latest/web/casian-doc`.


# First-time deployment
Don't forget to create the directories! (including `~/web/latest`)

```
mkdir -p ~/web/latest/web/casian-doc ~/web/latest/web/imp-doc ~/web/latest/web/tgm-pipeline
```

# Emergency recovery

## Elegant solution
  1. SSH into the VM, and remove all the old files `cd ~/web; rm -rf *`
  2. Recreate the folder structure with `mkdir -p ~/web/latest/web/casian-doc ~/web/latest/web/imp-doc ~/web/latest/web/tgm-pipeline`
  3. Go into r3-pages repository, and schedule CI/CD execution with `Run pipeline` -> master
  4. Go into respective pages repositories, and schedule their CI/CD execution with `Run pipeline` -> master

## Faster solution
There is a weekly backup, you might want to recover from it. The location of backups is in the `~/backups` directory.

  1. SSH into the VM, and remove all the old files `cd ~/web; rm -rf *`
  2. Unpack selected backup into `~/web/latest/`
