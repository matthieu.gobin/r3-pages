---
layout: default
date: 2021-11-12
order: -1
title: "Meta-analysis of gender-dependent gene expression alterations in Parkinson's disease"
permalink: /frozen/hpbx-y095
published: true
doi: 10.17881/hpbx-y095
type: other
---

{% rtitle {{ page.title }} %}
Léon-Charles Tranchevent, Rashi Halder, Enrico Glaab
{% endrtitle %}

{% rgridblock a-unique-id %}

{% rblock Raw data %}
The RNA-sequencing data is available through the <a href="https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE168496">NCBI GEO website</a>. A token is required during review, but the data will be publicly available after publication of the manuscript.
{% endrblock %}

{% rblock Supplementary data %}
The supplementary files associated with the manuscript are available <a href="https://webdav.lcsb.uni.lu/public/data/hpbx-y095">here</a>.
{% endrblock %}

{% rblock Source code %}
The source code used to make the analyses described in the publication is available <a href="https://gitlab.lcsb.uni.lu/bds/geneder/geneder_core">here</a>.
{% endrblock %}

{% endrgridblock %}




{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Other
{% endrfooter %}
