---
layout: default
date: 2020-12-11
order: -1
title: "Metadata for the RESOLUTE project in the IMI Data Catalog"
permalink: /frozen/tnyy-fy53
published: true
doi: 10.17881/tnyy-fy53
type: data
---


{% rtitle {{ page.title }} %}

Please refer to the IMI Data Catalog [here](https://datacatalog.elixir-luxembourg.org/).

Please cite the RESOLUTE project using [this Nature paper](https://media.nature.com/original/magazine-assets/d41573-020-00056-6/d41573-020-00056-6.pdf) (Giulio Superti-Furga, Daniel Lackner, Tabea Wiedmer, Alvaro Ingles-Prieto, the RESOLUTE consortium and Claire M. Steppan)


permalink: [doi:10.17881/th9v-xt85](https://doi.org/10.17881/tnyy-fy53)
{% endrtitle %}


{% rblock Metadata %}
The <b>RESOLUTE</b> project metadata is available in the IMI Data Catalog [here](https://datacatalog.elixir-luxembourg.org/e/dataset/79d2691a-104d-11ea-9e31-0050569a29db).
{% endrblock %}

{% rblock Documentation | fas fa-info %}
All available documentation can be found on [the RESOLUTE project website](https://re-solute.eu).
{% endrblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Dataset
{% endrfooter %}
