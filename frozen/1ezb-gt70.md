---
layout: default
date: 2023-07-25
order: -1
title: "Assessing the Maturity level of Wearable Sensors for Home Monitoring in Parkinson’s Disease through Evidence Evaluation Levels (EEL) and User Experience: A Comprehensive Review."
permalink: /frozen/1ezb-gt70
published: true
doi: 
type: 
---
{% rtitle {{ page.title }} %}
Stefano Sapienza, Olena Tsurkalenko, Marijus Giraitis, Alan Castro Mejia, Gelani Zelimkhanov, Isabel Schwaninger, Jochen Klucken

Abstract (253 words including bolded)
Introduction: Digital Medicine has witnessed significant advancements in the past two decades, primarily fueled by wearable sensors' emergence as remote monitoring tools. Wearables offer the potential for accurate, real-world, user-derived measurements across various medical applications, surpassing the limitations of traditional clinical evaluations. This paper focuses on wearable sensors in Parkinson's Disease (PD), where continuous home monitoring is crucial to understand the disease's multidimensional nature. However, the limited impact of wearable technology on PD's standard of care remains a concern, partly due to the need for a consistent metric to compare sensor performance and clinical applicability across studies.
Methods: This study investigate if the lack of tailored care evidence and scarce general usability in real- world scenarios is a critical element that limits the translation of research results into the patient’s journey. The levels include technical efficacy, diagnostic efficacy, clinical utility, usability, and adherence. We performed a systematic review of PubMed-hosted publications between January 2017 and May 2023, identifying 61 relevant studies for analysis. Utilizing the modified AXIS appraisal tool, we assessed the risk of bias in the selected articles.
Results: Our results indicate that wearables show promise in technical and diagnostic efficacy, but their clinical utility, usability, and adherence require further investigation. A third of the reviewed articles reported evidence of usability and adherence, highlighting their importance for successfully implementing wearable sensors in healthcare. Furthermore, heterogeneous methodologies are problematic for comparing results across studies.
Conclusion: This review highlight that a comprehensive evaluation of the maturity level of wearables would require a more complex approach that explores multiple domains in parallel to the user experience and the clinical utility of a tool. In such a way that it facilitates comparisons and enhances the clinical applicability of wearable sensor technologies in monitoring Parkinson's Disease patients in their home environment.
{% endrtitle %}

{% rgridblock a-unique-id %}
<!---
{% rblock Raw data %}
The raw data is available [here]().
{% endrblock %}

{% rblock Source code %}
The scripts used to analyse the data are available [here]().
{% endrblock %}
-->
{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}

{% endrfooter %}
