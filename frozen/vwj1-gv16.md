---
layout: default
date: 2023-05-10
order: -1
title: "Generation of two induced pluripotent stem cell lines and the corresponding isogenic controls from Parkinson’s disease patients carrying the heterozygous mutations c.815G>A (p.R272Q) or c.1348C>T (p.R450C) in the RHOT1 gene encoding Miro1"
permalink: /frozen/vwj1-gv16
published: true
doi: 10.17881/vwj1-gv16
type: Manuscript
---
{% rtitle {{ page.title }} %}
Axel Chemla, Giuseppe Arena, Gizem Onal, Jonas Walter, Clara Berenguer, Dajana Grossmann, Anne Grünewald, Jens Christian Schwamborn, Rejko Krüger

Fibroblasts from two Parkinson’s disease (PD) patients carrying either the heterozygous mutation c.815G>A (Miro1 p.R272Q) or c.1348C>T (Miro1 p.R450C) in the RHOT1 gene, were converted into induced pluripotent stem cells (iPSCs) using RNA-based and episomal reprogramming, respectively. The corresponding isogenic gene-corrected lines have been generated using CRISPR/Cas9 technology. These two isogenic pairs will be used to study Miro1-related molecular mechanisms underlying neurodegeneration in relevant iPSC-derived neuronal models (e.g., midbrain dopaminergic neurons and astrocytes)
{% endrtitle %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Other
{% endrfooter %}
