---
layout: default
date: 2023-03-28
order: -1
title: "Assessing the suitability of iPSC-derived human astrocytes for disease modeling"
permalink: /frozen/4jvc-fq34
published: true
doi: 10.17881/4jvc-fq34
type: manuscript
---
{% rtitle {{ page.title }} %}
Patrycja Mulica, Carmen Venegas, Zied Landoulsi, Katja Badanjak, Semra Smajic, Sylvie Delcambre, Jens Schwamborn, Rejko Krüger, Paul Antony, Patrick May, Enrico Glaab, Anne Grünewald, Sandro L. Pereira

Disease modeling with iPSC-derived cultures has proved to be particularly useful, although the selection of an appropriate protocol for a given research question remains challenging. Thus, here, we compared two approaches for the generation of iPSC-derived astrocytes. We phenotyped glia that were obtained using the differentiation protocols by Oksanen or Palm and colleagues, respectively. We employed high-throughput imaging and RNA sequencing to deep-characterize the cultures. Oksanen and Palm astrocytes differ considerably in their properties: while the former cells are more labor-intense in their generation (5 vs 2 months), they are also more mature. This notion was  strengthened by data resulting from cell type deconvolution analysis that was applied to bulk transcriptomes from the cultures to assess their similarity with human postmortem astrocytes. Overall, our analyses highlight the need to consider the advantages and disadvantages of a given differentiation protocol, when designing functional or drug discovery studies involving iPSC-derived astrocytes.

{% endrtitle %}

{% rblock Raw data | fas fa-database  %}

**Methodology**

- RNA sequencing data was generated at the Beijing Genomics Institute (BGI) in Copenhagen, Denmark, using the BGISEQ-500 platform 
- The dataset contains 2 healthy control cell lines 
- 3 biological replicates of each healthy control-derived astrocytes were sequenced, two methods of astrocyte differentiation were used as described in the manuscript

**Technical metadata**

- File formats:  FASTQ 
- Total size: 92 GB 
- 24 files, 12 samples

**Data Access**

The raw data for this manuscript is not available publicly due to its sensitive nature.

Requests for accessing the data can be directed to [anne.gruenewald@uni.lu](mailto:anne.gruenewald@uni.lu).
Data requests should include reference to the publication and intended data use. The request are assessed by local data access committee formed by senior research staff, ELSI experts and members of legal department. If required, the requester is expected to provide additional information on safeguards ensuring compliant processing of the data (e.g. ethics approval, institutional technical and organizational security measures).

After the data access committee approves the request, a Data Use Agreement is concluded between the provider and the recipient institution. The agreement includes definition of roles, general clauses on data protection and details on conditions of data use and its retention.

Data is finally transferred in encrypted form via secure channel. Checksums are shared along the data to ensure data integrity.
{% endrblock %}

{% rgridblock a-unique-id %}

{% rblock Derived data %}
All supplementary data and data behind figures are available [here](https://webdav.lcsb.uni.lu/public/data/4jvc-fq34/).
{% endrblock %}

{% rblock Source code %}
The scripts used to analyse the data are available [here](https://gitlab.lcsb.uni.lu/MFN/camesyn/comparison-protocols).
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | not yet published
{% endrfooter %}
