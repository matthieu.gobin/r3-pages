---
layout: default
date: 2023-08-16
order: -1
title: "Comprehensive blood metabolomics profiling of Parkinson’s disease reveals coordinated alterations in xanthine metabolism"
permalink: /frozen/wssf-d204
published: true
doi: 
type: 
---
{% rtitle {{ page.title }} %}
Elisa Gómez De Lope, Rebecca Ting Jiin Loo, Armin Rauschenberger, Muhammad Ali, Lukas Pavelka, Enrico Glaab

Parkinson's disease (PD) is a highly heterogeneous disorder with several environmental and
genetic factors contributing to the disease initiation and progression. Effective disease
modifying therapies and robust biomarker signatures for the early pre-motor and motor stages
of the disease are still lacking, and an improved understanding of the molecular changes
characterizing PD could help to reveal new diagnostic and prognostic markers and possible
targets for the study of pharmaceutical interventions.
Here, we report results from a cohort-wide blood plasma metabolic profiling of PD patients and
controls without neurodegenerative brain disorders in the Luxembourg Parkinson’s Study to
detect disease-associated alterations at the level of systemic cellular process and network
alterations. We identified statistically significant changes in both individual metabolite levels
and global pathway activities in PD vs. controls and significant correlations with motor
impairment scores. As a primary observation when investigating shared molecular sub-network
alterations, we detect pronounced and coordinated increased metabolite abundances in
xanthine metabolism, which are consistent with previous PD case/control transcriptomics data
from an independent cohort in terms of known enzyme-metabolite network relationships. From
the integrated metabolomics and transcriptomics network analysis, the enzyme hypoxanthine
phosphoribosyltransferase 1 (HPRT1) is determined as a potential key regulator controlling the
shared changes in xanthine metabolism and linking them to a mechanism that may contribute
to pathological loss of cellular ATP in PD.
Overall, the investigations revealed significant PD-associated metabolome alterations,
including pronounced changes in xanthine metabolism that are mechanistically congruent with
alterations observed in independent transcriptomics data. The enzyme HPRT1 may merit
further investigation as a main regulator of these network alterations and as a potential
therapeutic target to address downstream molecular pathology in PD.
{% endrtitle %}

{% rgridblock a-unique-id %}

{% rblock Raw data %}
The dataset for this manuscript is not publicly available as it is linked to the Luxembourg Parkinson’s Study and its internal regulations. Any requests for accessing the dataset can be directed to [request.ncer-pd@uni.lu](mailto:request.ncer-pd@uni.lu).
{% endrblock %}

{% rblock Source code %}
The scripts used to analyse the data are available [here](https://gitlab.lcsb.uni.lu/bds/pd_metabolomics).
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}

{% endrfooter %}
