---
layout: default
date: 2021-07-09
order: -1
title: "Synaptic decline precedes dopaminergic neuronal loss in human midbrain organoids harboring a triplication of the SNCA gene"
permalink: /frozen/1yzp-qv41
published: true
doi: 10.17881/1yzp-qv41
type: manuscript
manuscript: 10.1101/2021.07.15.452499
---

{% rtitle {{ page.title }} %}
Jennifer Modamio, Cláudia Saraiva, Gemma Gomez-Giro, Sarah Nickels, Javier Jarazo, Paul Antony, Silvia Bolognin, Peter Barbuti, Rashi Halder, Christian Jäger, Rejko Krueger, Enrico Glaab, Jens Schwamborn
{% endrtitle %}

{% rgridblock a-unique-id %}
{% rblock  Manuscript %}
A preprint of the manuscript is available <a href="https://www.biorxiv.org/content/10.1101/2021.07.15.452499v1">here</a>.
{% endrblock %}


{% rblock Data %}
The data is available <a href="https://webdav.lcsb.uni.lu/public/data/1yzp-qv41/">here</a>.
{% endrblock %}


{% rblock source code %}
The source code is available <a href="https://github.com/LCSB-DVB/Modamio_2021">here</a>.
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
