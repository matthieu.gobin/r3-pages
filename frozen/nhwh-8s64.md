---
layout: default
date: 2023-07-26
order: -1
title: "NORMAN Guidance on Suspect and Non-Target Screening in Environmental Monitoring"
permalink: /frozen/nhwh-8s64
published: true
doi: 10.17881/nhwh-8s64
type: manuscript
---
{% rtitle {{ page.title }} %}
Juliane Hollender, Emma Schymanski, Lutz Ahrens, Nikiforos Alygizakis, Frederic Béen, Lubertus Bijlsma, Andrea M. Brunner, Alberto Celma, Aurelie Fildier, Qiuguo Fu, Pablo Gago-Ferrero, Ruben Gil-Solsona, Peter Haglund, Martin Hansen, Sarit Kaserzon, Anneli Kruve, Marja Lamoree, Christelle Margoum, Jeroen Meijer, Sylvain Merel, Cassandra Rauert, Pawel Rostkowski, Saer Samanipour, Bastian Schulze, Tobias Schulze, Randolph Singh, Jaroslav Slobodnik, Teresa Steininger-Mairinger, Nikolaos S. Thomaidis, Anne Togola, Katrin Vorkamp, Emmanuelle Vulliet, Linyan Zhu, Martin Krauss

Increasing production and use of chemicals and awareness of their impact on ecosystems and humans has led to large interest for broadening the knowledge on the chemical status of the environment and human health by suspect and non-target screening (NTS). To facilitate effective implementation of NTS in scientific, commercial and governmental laboratories, as well as acceptance by managers, regulators and risk assessors, more harmonisation in NTS is required. To address this, NORMAN Association members involved in NTS activities have prepared this guidance document, based on the current state of knowledge. The document is intended to provide guidance on performing high quality NTS studies and data interpretation, while increasing awareness of the promise but also pitfalls and challenges associated with these techniques. Guidance is provided for all steps; from sampling and sample preparation to analysis by chromatography (liquid and gas - LC and GC) coupled via various ionisation techniques to high resolution tandem mass spectrometry (HRMS/MS), through to data evaluation and reporting in the context of NTS. Although most experience within the NORMAN network still involves water analysis of polar compounds using LC-HRMS/MS, other matrices (sediment, soil, biota, dust, air) and instrumentation (GC, ion mobility) are covered, reflecting the rapid development and extension of the field. Due to the ongoing developments, the different questions addressed with NTS and manifold techniques in use, NORMAN members feel that no standard operation process can be provided at this stage. However, appropriate analytical methods, data processing techniques and databases commonly compiled in NTS workflows are introduced, their limitations are discussed and recommendations for different cases are provided. Proper quality assurance, quantification without reference standards and reporting results with clear confidence of identification assignment complete the guidance together with a glossary of definitions. The NORMAN community greatly supports the sharing of experiences and data via open science and hope that this guideline supports this effort.
{% endrtitle %}

{% rgridblock a-unique-id %}


{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | not yet published
{% endrfooter %}
