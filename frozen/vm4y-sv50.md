---
layout: default
date: 2023-07-31
order: -1
title: "Defects in Miro1 cause impaired mitochondrial homeostasis and degeneration of human and murine dopaminergic neurons in vitro and in vivo"
permalink: /frozen/vm4y-sv50
published: true
doi: 
type: 
---
{% rtitle {{ page.title }} %}
Axel Chemla, Giuseppe Arena, Ginevra Sacripanti, Kyriaki Barmpa, Alise Zagare, François Massart, Paul Antony, Jochen Ohnmacht, Pierre Garcia, Jaqueline Jung, Anne-Marie Marzesco, Thorsten Schmidt, Anne Grünewald, Jens Christian Schwamborn, Rejko Krüger, Claudia Saraiva

Miro1 is a cytosolic calcium sensor involved in the maintenance of mitochondrial homeostasis and quality control. We previously identified PD patients carrying heterozygous RHOT1 variants and demonstrated, that Miro1 mutations impaired calcium homeostasis and dysregulated cellular and mitochondrial quality control. To explore the involvement of PD-associated Miro1 variants in neurodegeneration, we used iPSC-derived neurons and midbrain organoids from a PD patient carrying the heterozygous p.R272Q Miro1 mutation. We found disrupted metabolism and impaired mitochondrial bioenergetics, leading to neurons needing to clear dysfunctional mitochondria by mitophagy. Dysfunctional mitophagy might have led to impaired α-synuclein levels clearance and dopaminergic neurons (DAN) death. DANs loss was confirmed in the substantia nigra of mice expressing the ortholog Miro1 mutation, accompanied by impaired anterograde procedural memory. Our findings demonstrate that p.R272Q Miro1 is sufficient to comprehensively model PD-relevant cellular phenotypes in vitro and in vivo, reinforcing the role of Miro1 in dopaminergic neurodegeneration in PD.
{% endrtitle %}

{% rgridblock a-unique-id %}
<!---
{% rblock Raw data %}
The raw data is available [here]().
{% endrblock %}

{% rblock Source code %}
The scripts used to analyse the data are available [here]().
{% endrblock %}
-->
{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}

{% endrfooter %}
