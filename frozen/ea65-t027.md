---
layout: default
date: 2023-08-02
order: -1
title: "Genetic landscape of Parkinson’s disease in Luxembourg"
permalink: /frozen/ea65-t027
published: true
doi: 
type: 
---
{% rtitle {{ page.title }} %}
Zied Landoulsi, Sinthuja Pachchek, Dheeraj Reddy Bobbili, Lukas Pavelka, Patrick May, Rejko Krüger

Objectives: To explore the genetics of PD in a case-control Luxembourgish cohort including patients with both typical and atypical PD.
Methods: A Luxembourg cohort of 809 healthy controls, 680 PD and 103 patients with atypical PD were genotyped using the Neurochip array. We screened and validated rare single nucleotide variants (SNVs) and copy number variants (CNVs) within selected PD-causing genes. Polygenic risk scores (PRSs) were generated using the latest genome-wide association study for PD. We then estimated the role of common variants in PD risk by applying gene-set-specific PRSs.
Results: We identified 60 rare SNVs in seven PD-causing genes, nine of which were pathogenic in LRRK2, PINK1 and PRKN. Eleven rare CNVs were detected in PRKN including seven duplications and four deletions. The majority of PRKN SNVs and CNVs carriers were heterozygous and not significantly distributed between cases and controls. The PRS was significantly associated with PD status and specific molecular processes related to protein metabolism and signal transduction were among the main drivers of PD risk.
Conclusion: We performed a descriptive genetic characterization of the deeply phenotypes Luxembourgish PD case-control cohort, screening for rare SNVs, rare CNVs and low-risk SNPs effects, which will help future studies to unravel the genetic complexity of PD.
{% endrtitle %}

{% rgridblock a-unique-id %}

{% rblock Raw and Derived data %}
The dataset for this manuscript is not publicly available as it is linked to the Luxembourg Parkinson’s Study and its internal regulations. Any requests for accessing the dataset can be directed to [request.ncer-pd@uni.lu](mailto:request.ncer-pd@uni.lu]).
{% endrblock %}

{% rblock Source code %}
The scripts used to analyse the data are available [here](https://gitlab.lcsb.uni.lu/genomeanalysis/ncer/luxpark_genetic_landscape/).
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}

{% endrfooter %}
