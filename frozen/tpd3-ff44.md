---
layout: default
date: 2023-05-12
order: -1
title: "Visualization of automatically combined disease maps and pathway diagrams for rare diseases"
permalink: /frozen/tpd3-ff44
published: true
doi: 
type: 
---
{% rtitle {{ page.title }} %}
Piotr Gawron, David Hoksza, Janet Piñero, Maria Pena Chilet, Marina Esteban, Jose Luis Fernandez-Rueda,Vincenza Colonna, Ewa Smula, Laurent Heirendt, François AncienValentin GrouesVenkata Pardhasaradhi SatagopamReinhard SchneiderJoaquin Dopazo Laura I. Furlong, Marek Ostaszewski

Investigation of molecular mechanisms of human disorders, especially rare diseases, require exploration of various knowledge repositories for building precise hypotheses and complex data interpretation. Recently, increasingly more resources offer diagrammatic representation of such mechanisms, including disease-dedicated schematics in pathway databases and disease maps. However, collection of knowledge across them is challenging, especially for research projects with limited manpower. In this article we present an automated workflow for construction of maps of molecular mechanisms for rare diseases. The workflow requires a standardized definition of a disease using OrphaNet or HPO identifiers to collect relevant genes and variants, and to assemble a functional, visual repository of related mechanisms, including data overlays. The diagrams composing the final map are unified to a common systems biology format from CellDesigner SBML, GPML and SBML+layout+renders. The constructed resource contains disease-relevant genes and variants as data overlays for immediate visual exploration, including embedded genetic variant browser and protein structure viewer. We demonstrate the functionality of our workflow on an example of Kawasaki disease. In summary, our work allows for an ad-hoc construction of molecular diagrams combined from different sources, preserving their layout and graphical style, but integrating them into a single resource. This allows to reduce time consuming tasks of prototyping of a molecular disease map, enabling visual exploration, hypothesis building, data visualization and further refinement. The code of the workflow is open and accessible at https://gitlab.lcsb.uni.lu/minerva/automap/.
{% endrtitle %}

{% rgridblock a-unique-id %}

{% rblock Raw data %}
The datasets are available [here](https://pathwaylab.elixir-luxembourg.org/minerva/export.xhtml?id=adhoc_ORPHA2331) and [here](https://pathwaylab.elixir-luxembourg.org/minerva/export.xhtml?id=adhoc_ORPHA791).
{% endrblock %}

{% rblock Source code %}
The scripts used to analyse the data are available [here](https://gitlab.lcsb.uni.lu/minerva/automap/).
{% endrblock %}

{% rblock Web service %}
A workflow to create the disease map on the file is available under [automap.elixir-luxembourg.org](https://automap.elixir-luxembourg.org).
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}

{% endrfooter %}
