---
layout: default
date: 2023-08-21
order: -1
title: "ShinyTPs: Curating transformation products from text mining results"
permalink: /frozen/dhtx-4m12
published: true
doi: 
type: 
---
{% rtitle {{ page.title }} %}
Emma Helena Palm, Parviel Chirsir, Jessy Krier, Paul A. Thiessen, Jian Zhang, Evan E. Bolton, Emma Schymanski

Transformation product (TP) information is essential to accurately evaluate the hazard
compounds pose to human health and the environment. However, information about
transformation products (TPs) is often limited, and existing data is often not fully Findable,
Accessible, Interoperable and Reusable (FAIR). FAIRifying existing TP knowledge is a relatively
easy path towards improving access to data for identification workflows and machine learning-
based algorithms alike. ShinyTPs has been developed to curate existing transformation
information derived from text mined data within the PubChem database. The app (available as
an R package) visualizes the text-mined chemical names to facilitate user validation of the
automatically extracted reactions.
ShinyTPs was applied to a case study using 436 tentatively identified compounds to prioritize TP retrieval. This resulted in 645 (associated with 496
compounds), of which 319 were not previously available.
in the same water sample using patron. In total 74 compounds from the library were identified
The curated reactions were added to
the PubChem transformations library which was used as a TP suspect list for identification of TP
25% of which were curated using ShinyTPs showing that the app can significantly improve TP
identification in non-target analysis workflows.
{% endrtitle %}

{% rgridblock a-unique-id %}

{% rblock Raw data %}
The raw data is available [here](https://zenodo.org/record/7838005).
{% endrblock %}

{% rblock Source code %}
The scripts used to analyse the data are available [here](https://gitlab.lcsb.uni.lu/eci/shinytps).
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}

{% endrfooter %}
