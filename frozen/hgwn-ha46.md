---
layout: default
date: 2023-07-06
order: -1
title: "An improved PDE6D inhibitor combines with Sildenafil to synergistically inhibit KRAS mutant cancer cell growth"
permalink: /frozen/hgwn-ha46
published: true
doi: 
type: 
---
{% rtitle {{ page.title }} %}
Pelin Kaya, Elisabeth Schaffner-Reckinger, Ganesh Babu Manoharan, Vladimir Vukic, Alexandros Kiriazis, Mirko Ledda, Maria Burgos, Karolina Pavic, Anthoula Gaigneaux, Enrico Glaab, Daniel Kwaku Abankwa

The activity of inhibitors against the trafficking chaperone PDE6D (or PDE𝛿) in KRAS-mutant cancer cells has remained below expectations. We here developed new nanomolar PDE6D-inhibitors (PDE6Di) with improved on-target profiles, as assessed by a dedicated analysis in PDE6D-knockout MEFs and in an assay testing for the intracellular engagement of the PDE6D-related UNC119A. Our results demonstrate that a higher affinity to the hydrophobic pocket of PDE6D creates both solubility and off-target issues. In order to further improve the K-Ras selectivity of our highly soluble compound, Deltaflexin3, we combined it with Sildenafil. Sildenafil stimulates Ser181-phophorylation and thus loss of PDE6D-affinity of K-Ras. Thus, the combination robustly reduces Ras-signaling, and synergistically inhibits the proliferation of cancer cells and ex vivo tumor growth. Our work provides the currently most selective and highly soluble PDE6Di and suggests synergistic combinations to focus the inhibition on K-Ras.
{% endrtitle %}

{% rgridblock a-unique-id %}

{% rblock Raw data %}
All data can be retrieved from the [TCGA Pan-Cancer Atlas](https://dev.xenabrowser.net/datapages/?cohort=TCGA%20Pan-Cancer%20). Non-silent somatic mutations are used and defined on the [UCSC Xena Page](https://ucsc-xena.gitbook.io/project/overview-of-features/visual-spreadsheet/mutation-columns)
{% endrblock %}

<!---
{% rblock Source code %}
The scripts used to analyse the data are available [here]().
{% endrblock %}
-->
{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}

{% endrfooter %}
