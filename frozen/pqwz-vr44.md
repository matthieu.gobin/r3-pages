---
layout: default
date: 2023-05-16
order: -1
title: "CuFluxSampler.jl: Flexible GPU-accelerated flux samplers for metabolic modeling"
permalink: /frozen/pqwz-vr44
published: true
doi: 
type: 
---
{% rtitle {{ page.title }} %}
Miroslav Kratochvil, St. Elmo Wilken, Venkata Pardhasaradhi Satagopam, Reinhard Schneider, Wei Gu, Christophe Trefois

Flux sampling is a powerful technique for analyzing the feasible spaces of constraint-based metabolic models. We present CuFluxSampler.jl, a collection of GPU-accelerated flux sampling algorithms that aid quick exploration of complicated constraint-based metabolic models. CuFluxSampler.jl is built upon COBREXA.jl, and shares most of its design features, making it easy to run massively parallel analyses of models subjected to various conditions and constraints. We demonstrate this by exploring and visualizing whole ensembles of metabolic flux samples, showing the ability to build comprehensive flux maps of the model with respect to proteomic and genomic constraints. Finally, we detail the architecture of the CUDA-based implementation and its utilization in HPC platforms. 
{% endrtitle %}

{% rgridblock a-unique-id %}

{% rblock Source code %}
The data and scripts used to generate the results are available [here](https://gitlab.lcsb.uni.lu/lcsb-biocore/cufluxsampler-scripts).
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}

{% endrfooter %}
