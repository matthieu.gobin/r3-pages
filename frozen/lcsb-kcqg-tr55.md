---
layout: default
date: 2020-03-09
order: -1
title: "Using High-Content Screening to Generate Single-Cell Gene-Corrected Patient-Derived iPS Clones Reveals Excess Alpha-Synuclein with Familial Parkinson’s Disease Point Mutation A30P"
permalink: /frozen/lcsb-kcqg-tr55
published: true
doi: 10.17881/lcsb.kcqg.tr55
type: manuscript
manuscript: 10.3390/cells9092065
redirect_from:
 - /frozen/screening
 - /frozen/screening/
---

{% rtitle {{ page.title }} %}
Peter A. Barbuti, Paul M. Antony, Bruno FR. Santos, François Massart, Gérald Cruciani, Claire M. Dording, Jonathan Arias, Jens C. Schwamborn, Rejko Krüger
<center>
<img src="{{ "assets/lcsb-kcqg-tr55/screening.png" | relative_url }}" width="90%" />
</center>
{% endrtitle %}

{% rgridblock a-unique-id %}
{% rblock  Raw data | fas fa-video %}
The complete dataset is available [here](https://webdav.lcsb.uni.lu/public/data/screening/).
{% endrblock %}

{% rblock source code %}
The source code used to make the analysis/figures of the publication is available on [Gitlab](https://gitlab.lcsb.uni.lu/LCSB-CEN/cloneclassifier).
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
