---
layout: default
date: 2020-01-22
order: -1
title: "Passive controlled flow for neuronal cell culture in 3D microfluidic devices"
permalink: /frozen/lcsb-a8hh-1022
published: true
doi: 10.17881/lcsb.a8hh.1022
type: manuscript
manuscript: 10.1016/j.ooc.2020.100005
redirect_from:
 - /frozen/passive-controlled-flow-for-neuronal-cell-culture-in-3d-microfluidic-devices
 - /frozen/passive-controlled-flow-for-neuronal-cell-culture-in-3d-microfluidic-devices/
---

{% rtitle {{ page.title }} %}
Khalid I.W. Kane, Javier Jarazo, Edinson Lucumi Moreno, Paul Vulto, Ronan M.T. Fleming , Bas Trietsch, Jens C. Schwamborn*
{% endrtitle %}


{% rgridblock a-unique-id %}
{% rblock  Raw data | fas fa-image %}
The complete **Dataset** is available [here](https://webdav.lcsb.uni.lu/public/data/passive-controlled-flow-for-neuronal-cell-culture-in-3d-microfluidic-devices/). It is subdivided into originals (raw data) and partials (analysis) specific to each figure and supplementary present in the manuscript.
{% endrblock %}


{% endrgridblock %}



{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
