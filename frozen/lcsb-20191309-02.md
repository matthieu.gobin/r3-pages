---
layout: default
date: 2019-09-13
order: -1
title: "Machine learning-assisted neurotoxicity prediction in human midbrain organoids"
permalink: /frozen/lcsb-20191309-02
published: true
doi: 10.17881/lcsb.20191309.02
type: manuscript
manuscript: 10.1016/j.parkreldis.2020.05.011
redirect_from:
 - /frozen/machine-learning-assisted-neurotoxicity-prediction-in-human-midbrain-organoids/
 - /frozen/machine-learning-assisted-neurotoxicity-prediction-in-human-midbrain-organoids
---


{% rtitle {{ page.title }} %}
Please cite the article on [Paper](#).

Anna S. Monzel, Kathrin Hemmer, Tony Kaoma Mukendi, Philippe Lucarelli, Isabel Rosety, Alise Zagare, Silvia Bolognin, Paul Antony, Sarah L. Nickels, Rejko Krueger, Francisco Azuaje, Jens C. Schwamborn*
{% endrtitle %}


{% rgridblock a-unique-id %}
{% rblock  SOURCE CODE | fas fa-code %}
The source code is available on [Gitlab](https://gitlab.lcsb.uni.lu/dvb/Monzel_2020) where you can traceback what has been done by the authors. Similarly, you can find the code used for the machine learning analysis [here](https://gitlab.com/biomodlih/tox-ml).
{% endrblock %}

{% rblock  Figures | fas fa-image %}
The data for the figures can be found [here](https://webdav.lcsb.uni.lu/public/data/machine-learning-assisted-neurotoxicity-prediction-in-human-midbrain-organoid/).
{% endrblock %}

{% endrgridblock %}




{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
