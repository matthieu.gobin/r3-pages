---
layout: default
date: 2019-03-25
order: -1
title: "Single-cell transcriptomics reveals multiple neuronal cell types in human midbrain-specific organoids"
permalink: /frozen/lcsb-20190326-01
published: true
doi: 10.17881/lcsb.20190326.01
type: manuscript
manuscript: 10.1007/s00441-020-03249-y
redirect_from:
 - /frozen/single-cell-transcriptomics-reveals-multiple-neuronal-cell-types-in-human-midbrain-specific-organoids/
 - /frozen/single-cell-transcriptomics-reveals-multiple-neuronal-cell-types-in-human-midbrain-specific-organoids
---


{% rtitle {{ page.title }} %}
Please cite the article on [BioRxiv](https://www.biorxiv.org/content/10.1101/589598v1).

Lisa M Smits, Magni Stefano, Grzyb Kamil, Antony Paul MA, Krüger Rejko, Skupin Alexander, Bolognin Silvia, Jens C Schwamborn*
{% endrtitle %}


{% rgridblock a-unique-id %}
{% rblock  SOURCE CODE | fas fa-code %}
The source code used to make the publication is available on [Gitlab](https://github.com/LCSB-DVB/Smits_Reinhardt_2019) where you can traceback what have been done by the authors.
{% endrblock %}

{% rblock  SINGLE-CELL TRANSCRIPTOMICS | fas fa-th %}
Data is accessible through [LCSB WebDAV](https://webdav.lcsb.uni.lu/public/data/single-cell-transcriptomics-reveals-multiple-neuronal-cell-types-in-human-midbrain-specific-organoids/single-cell_transcriptomics/) website.
{% endrblock %}

{% rblock  RAW DATA | fas fa-image %}
The complete **Dataset** is available [here](https://webdav.lcsb.uni.lu/public/data/single-cell-transcriptomics-reveals-multiple-neuronal-cell-types-in-human-midbrain-specific-organoids/raw-data/).
{% endrblock %}
{% endrgridblock %}




{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
